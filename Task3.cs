using System;

class NumVector
{
	private int _length;
	private int _offset;
	private int _elementsCount;
	private int?[] _numArray;
	//if you want to use custom index ranges like [6,12] you should use constructor with 2 parameters setting length and offset to 0. for exaple if you 
	//want array with indexes 6,7,8,9,10,11,12 and 6 elements it looks like NumVector(6,6);. When using cycles, startting point is offset, ending length
	//when using without offset just use constructor with 1 parameter (length) and when cycles use elementsCount.
	NumVector(int length, int offset)
	{
		_elementsCount = length;
		_length = length + offset;
		_offset = offset;
		_numArray = new int?[_length];
	}
	
	NumVector(int length)
	{
		_elementsCount = length;
		_length = length;
		_offset = 0;
		_numArray = new int?[_length];
	}
	
	public int? this[int i]
	{
		get
		{
			try
			{
				return _numArray[i - _offset];
			}
			catch (System.IndexOutOfRangeException ex)
			{
				Console.WriteLine("Index is out of range");
				return null;
			}
		}
		set
		{
			try
			{
				_numArray[i - _offset] = value;
			}
			catch (System.IndexOutOfRangeException ex)
			{
				Console.WriteLine("Index is out of range.");
			}
		}
	}
	
	public static NumVector operator + (NumVector v1, NumVector v2)
	{
		if (v1._elementsCount == v2._elementsCount)
		{
			var temp = new NumVector(v1._elementsCount);
			for (int i = 0; i < v1._elementsCount; i++)
			{
				temp[i] = v1[i + v1._offset] + v2[i + v2._offset];
			}
			
			return temp;
		}
		else
		{
			Console.WriteLine("Error. Vector's length is not equal");
			return null;
		}
	}
	
	public static NumVector operator - (NumVector v1, NumVector v2)
	{
		if (v1._elementsCount == v2._elementsCount)
		{
			var temp = new NumVector(v1._elementsCount);
			for (int i = 0; i < v1._elementsCount; i++)
			{
				temp[i] = v1[i + v1._offset] - v2[i + v2._offset];
			}
			
			return temp;
		}
		else
		{
			Console.WriteLine("Error. Vector's length is not equal");
			return null;
		}
	}
	
	public void MultiplyVector(int num)
	{
		for(int i = _offset; i < _length; i++)
		{
			_numArray[i] *= num;
		}
	}
	
	public static bool operator == (NumVector v1, NumVector v2)
	{
		if (v1._elementsCount == v2._elementsCount)
		{
			bool flag = true;
			
			for (int i = 0; i < v1._elementsCount; i++)
			{
				if(v1[i + v1._offset] != v2[i + v2._offset])
				{
					flag = false;
					break;
				}
			}
			
			if(flag)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}
	
	public static bool operator != (NumVector v1, NumVector v2)
	{
		if (v1._elementsCount == v2._elementsCount)
		{
			bool flag = true;
			
			for (int i = 0; i < v1._elementsCount; i++)
			{
				if(v1[i + v1._offset] != v2[i + v2._offset])
				{
					flag = false;
					break;
				}
			}
			
			if(flag)
			{
				return false;
			}
			else
			{
				return true;
			}
		}
		else
		{
			return true;
		}
	}
	
	public static bool operator > (NumVector v1, NumVector v2)
	{
		if (v1._elementsCount == v2._elementsCount)
		{
			bool flag = true;
			
			for (int i = 0; i < v1._elementsCount; i++)
			{
				if(v1[i + v1._offset] < v2[i + v2._offset] || v1[i + v1._offset] == v2[i + v2._offset])
				{
					flag = false;
					break;
				}
			}
			
			if(flag)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}
	
	public static bool operator >= (NumVector v1, NumVector v2)
	{
		if (v1._elementsCount == v2._elementsCount)
		{
			bool flag = true;
			
			for (int i = 0; i < v1._elementsCount; i++)
			{
				if(v1[i + v1._offset] < v2[i + v2._offset])
				{
					flag = false;
					break;
				}
			}
			
			if(flag)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}
	
	public static bool operator < (NumVector v1, NumVector v2)
	{
		if (v1._elementsCount == v2._elementsCount)
		{
			bool flag = true;
			
			for (int i = 0; i < v1._elementsCount; i++)
			{
				if(v1[i + v1._offset] > v2[i + v2._offset] || v1[i + v1._offset] == v2[i + v2._offset])
				{
					flag = false;
					break;
				}
			}
			
			if(flag)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}
	
	public static bool operator <= (NumVector v1, NumVector v2)
	{
		if (v1._elementsCount == v2._elementsCount)
		{
			bool flag = true;
			
			for (int i = 0; i < v1._elementsCount; i++)
			{
				if(v1[i + v1._offset] > v2[i + v2._offset])
				{
					flag = false;
					break;
				}
			}
			
			if(flag)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}
	
	static void Main()
	{
		var numVect1 = new NumVector(6);
		var numVect2 = new NumVector(6,6);
		var numVect3 = new NumVector(6);
		
		Console.WriteLine("Vector 1");
		
		for(int i = numVect1._offset; i < numVect1._length; i++)
		{
			numVect1[i] = i;
			Console.Write("{0} ", numVect1[i]);  
		}
		
		Console.WriteLine("\nVector 2");
		
		for(int i = numVect2._offset; i < numVect2._length; i++)
		{
			numVect2[i] = i;
			Console.Write("{0} ", numVect2[i]);  
		}
		
		Console.WriteLine("\nVector 3");
		
		for(int i = numVect3._offset; i < numVect3._length; i++)
		{
			numVect3[i] = i;
			Console.Write("{0} ", numVect3[i]);  
		}
		
		Console.WriteLine("\nMultiply vector2 by number (3)");
		
		numVect2.MultiplyVector(3);		
				
		for(int i = numVect2._offset; i < numVect2._length; i++)
		{
			numVect2[i] = i;
			Console.Write("{0} ", numVect2[i]);  
		}
		
		if(numVect2 > numVect1)
		{
			Console.WriteLine("\nVector 2 is bigger than vector 1");
		}
		
		if(numVect3 >= numVect1)
		{
			Console.WriteLine("\nVector 3 is bigger/equal than vector 1");
		}
		
		if(numVect1 == numVect3)
		{
			Console.WriteLine("\nVector 1 is equal to vector 3");
		}
		
		if(numVect1 != numVect2)
		{
			Console.WriteLine("\nVector 1 is not equal to vector2");
		}
		
		var addTest = numVect1 + numVect2;
		var subTest = numVect2 - numVect1;
		
		Console.WriteLine("\nAdd test result. Adding vector1 and vector2");
		for(int i = 0; i < addTest._elementsCount; i++)
		{
			Console.Write("{0} ", addTest[i]);  
		}
		
		Console.WriteLine("\nSub test result. Sub vector2 and vector1");
		for(int i = 0; i < subTest._elementsCount; i++)
		{
			Console.Write("{0} ", subTest[i]);  
		}
	
		Console.ReadLine();
	}
}